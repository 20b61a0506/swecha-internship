#Arithrmetic operations
a = int(input("enter the first number\t:"))
b = int(input("enter the second number\t:"))
# addition
print ('Sum: ', a + b)  
# subtraction
print ('Subtraction: ', a - b)   
# multiplication
print ('Multiplication: ', a * b)  
# division
print ('Division: ', a / b) 
# floor division
print ('Floor Division: ', a // b)
# modulo division
print ('Modulo Division: ', a % b)  
# a to the power b
print ('Power: ', a ** b)   

#assignment operation
a += b  # a = a + b
print(a)
a*=b
print(a)
a|=b
print(a)
a-=b
print(a)

#comparision operations
# equal to operator
print('a == b =', a == b)
# not equal to operator
print('a != b =', a != b)
# greater than operator
print('a > b =', a > b)
# less than operator
print('a < b =', a < b)
# greater than or equal to operator
print('a >= b =', a >= b)
# less than or equal to operator
print('a <= b =', a <= b)

#logical operations
print((a >2) and (b >= 6))
print((a >2) or (b >= 6))
print(not(a))
