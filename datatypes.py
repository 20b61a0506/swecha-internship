#list tuple set dictionary
# Python3 program for explaining
# use of list, tuple, set and
# dictionary

# Lists
l = []

# Adding Element into list
l.append(15)
l.append(20)
print("Adding 15 and 20 in list", l)

# Set
s = set()

# Adding element into set
s.add(5)
s.add(10)
print("Adding 5 and 10 in set", s)

# Tuple
t = tuple(l)

# Tuples are immutable
print("Tuple", t)
print()

# Dictionary
d = {}
# dictionary with keys and values of different data types
numbers = {8: "eight", 1: "one", 7: "seven"}
print(numbers)
