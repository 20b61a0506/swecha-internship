#identity operations
x1 = 5
y1 = 5
x2 = 'Hello'
y2 = 'Hello'
x3 = [1,2,3]
y3 = [1,2,3]
print(x1 isnot y1)  # prints False
print(x2 is y2)  # prints True
print(x3 is y3)  # prints False

#membership operations
x = 'Hello world'
y = {1:'a', 2:'b'}
# check if 'H' is present in x string
print('H'in x)  # prints True
# check if 'hello' is present in x string
print('hello'notin x)  # prints True
# check if '1' key is present in y
print(1in y)  # prints True
# check if 'a' key is present in y
print('a'in y)  # prints False
