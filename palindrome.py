functionisPalindrome(str) {
  // Remove non-alphanumeric characters and convert to lowercase
  str=str.replace(/[^a-zA-Z0-9]/g, '').toLowerCase();
  
  // Compare characters from both ends
  letstart=0;
  letend=str.length-1;
  
  while (start<end) {
    if (str[start] !==str[end]) {
      returnfalse; // Characters don't match, not a palindrome
    }
    start++;
    end--;
  }
  
  returntrue; // All characters match, it's a palindrome
}

// Example usage:
console.log(isPalindrome("level")); // true
console.log(isPalindrome("hello")); // false
console.log(isPalindrome("A man, a plan, a canal: Panama")); // true

